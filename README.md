![](https://bitbucket.org/ram6ler/ti-thugs-bowmen/wiki/tnb.png)

Welcome to _Thugs and Bowmen_, a combinatorics guessing game in the same family as [Mastermind](http://en.wikipedia.org/wiki/Mastermind_(board_game)) and [Bulls and Cows](http://en.wikipedia.org/wiki/Bulls_and_cows) that is based on the rules of [_roshambo_ (rock-paper-scissors)](http://en.wikipedia.org/wiki/Rock-paper-scissors) and is playable on the Texas Instruments graphic display calculator model TI-92/V200.

Please see the [repo wiki](https://bitbucket.org/ram6ler/ti-thugs-bowmen/wiki/Home) for more information.